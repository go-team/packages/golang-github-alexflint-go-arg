Source: golang-github-alexflint-go-arg
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Nilesh Patra <nilesh@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-alexflint-go-scalar-dev,
               golang-github-stretchr-testify-dev,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-alexflint-go-arg
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-alexflint-go-arg.git
Homepage: https://github.com/alexflint/go-arg
XS-Go-Import-Path: github.com/alexflint/go-arg

Package: golang-github-alexflint-go-arg-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-alexflint-go-scalar-dev,
         ${misc:Depends}
Description: Struct-based argument parsing in Go
 The idea behind go-arg is that Go already has an excellent way
 to describe data structures using structs, so there is no need to
 develop additional levels of abstraction. Instead of one API to specify
 which arguments a program accepts, and then another API to get the
 values of those arguments, go-arg replaces both with a single struct.
